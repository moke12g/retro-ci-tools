#! /usr/bin/python3
import sys
import time
from vncdotool import api
from PIL import Image

if __name__ == "__main__":
    port = sys.argv[1]
    scheduled_runs = int(sys.argv[2])
    scheduled_runs *= 2 # turns it into minutes
    print("Doing " + str(scheduled_runs) + " runs")
    runs = -1
    print("Connecting to " + '127.0.0.1:' + port)
    client = api.connect('127.0.0.1:' + port, password=None)
    while runs < scheduled_runs:
        time.sleep(30)
        # capture screen
        print("Capture screen")
        client.captureScreen('screenshot.png')
        # check for color
        if Image.open('screenshot.png').convert('RGB').getpixel((200, 200)) == (255, 0, 255):
            print("Fuchsia found. It worked!")
            runs = scheduled_runs
        else:
            print("New Round " + str(runs) + " / " + str(scheduled_runs))
        runs += 1
    api.shutdown()
    if not Image.open('screenshot.png').convert('RGB').getpixel([200, 200]) == (255, 0, 255):
        print("Fuchsia was not detected. Something went wrong.")
        sys.exit(1)
    print("Have a nice day!")
    sys.exit(0)
